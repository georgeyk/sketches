# sketches

My p5js sketches.

Most of it should be available at [openprocessing](https://openprocessing.org/user/280979/).

This repo is more of a backup or experiments prior to the sketch publication.

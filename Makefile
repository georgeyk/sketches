P5_MANAGER:="${PWD}/node_modules/p5-manager/bin/p5-manager.js"
SKETCHES_DIR:=sketches

server:
	@cd $(SKETCHES_DIR) && $(P5_MANAGER) server -p 5000

new-sketch:
	@cd $(SKETCHES_DIR) && $(P5_MANAGER) generate $(NAME)

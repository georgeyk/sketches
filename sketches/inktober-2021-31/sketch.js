/*
 Inktober 2021
 Prompt: Risk
*/

let interval = 1;
let sparking = true;
let path;

function setup() {
  createCanvas(800, 600);
  frameRate(30);
  path = xline();
}

function draw() {
  background(104, 150, 137);

  if (path.length <= 1) {
    restart();
  }

  push();
  stroke(28, 48, 65);
  strokeWeight(3);

  for (let i = 1; i < path.length; i++) {
    point(path[i].x, path[i].y);
  }
  pop();

  spark(path[0].x, path[0].y, 30);
  path.splice(0, 1);
}


function restart() {
  path = xline();
  redraw();
}

function mouseClicked() {
  restart();
}


function xline() {
  let points = [];
  let angle = 0;

  let m = random(30, 120);
  for (let w = 100; w < 700; w++) {
    let y = w + sin(angle) * m;
    points.push(createVector(w, y));

    angle += 0.01;
  }
  return points;
}


function spark(x, y, size) {
  let clrs = [
    color(200),
    color(252, 194, 0),
    color(244, 211, 94),
  ];

  for (let i = 0; i < 50; i++) {
    let angle = random(TWO_PI);
    let x1 = x + cos(angle) * random(1, size);
    let y1 = y + sin(angle) * random(1, size);

    let c = random(clrs);
    stroke(c);
    fill(c);
    line(x, y, x1, y1);

    if (random() > 0.9) {
      strokeWeight(0.75);
      ellipse(x1 + random(-10, 10), y1-random(-10, 10), random(5));
    }

  }
}

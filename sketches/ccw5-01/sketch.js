/*
George Y. Kussumoto
EDX - Creative Coding - NYUx DMEDX6063
Week 5 - Sept/2021

Prompt:
Using your own custom function, draw a character (animal, person, etc.). Your character function must use function
arguments, allowing you to pass in positionX, positionY, characterWidth, characterHeight, and characterColor
properties.
*/

function setup() {
  createCanvas(windowWidth, windowHeight);
  colorMode(HSB);
  noLoop();
}

function draw() {
  pair = get_color_pair();
  background(pair[0]);

  ghost_width = windowWidth * random(0.2, 0.6);
  ghost_height = windowHeight * random(0.3, 0.8);
  // offsets to align on the center
  gx = windowWidth/2 - ghost_width/2;
  gy = windowHeight/2 - ghost_height/2;

  ghost(gx, gy, ghost_width, ghost_height, pair[1]);
}


function mouseClicked() {
  redraw();
}


function get_color_pair() {
  let bg_hue = random(1, 360);
  let fg_hue;

  if (bg_hue > 180) {
    fg_hue = bg_hue - 180;
  }
  else {
    fg_hue = bg_hue + 180;
  }

  // complemetary, but maybe not so aggressive on the eyes
  return [color(bg_hue, 30, 70), color(fg_hue, 40, 80)];
}


function ghost(gx, gy, gwidth, gheight, gcolor) {
  // to really understand what is happening, draw the rect surround the whole "ghost"
  // everything is basically an offset or percentage of rectangle parts
  // rect(gx, gy, gwidth, gheight);

  body(gx, gy, gwidth, gheight, gcolor);

  let dir = random(['UP', 'DOWN', 'LEFT', 'RIGHT', 'CENTER']);
  ewidth = gwidth * random(0.10, 0.35);
  eheight = gheight * random(0.15, 0.45);
  ex = gx + gwidth/2;
  ey = gy + gheight/3;

  // "10" is a simple spacing between the eyes
  eye(ex - 10 - ewidth/2, ey, ewidth, eheight, dir);
  eye(ex + 10 + ewidth/2, ey, ewidth, eheight, dir);
}


function body(bx, by, bwidth, bheight, bcolor) {
  // body = ellipse on the top + sine wave on the bottom
  push();
  noStroke();
  fill(bcolor);

  arc(bx + bwidth/2, by + bheight/3, bwidth, bheight - bheight/3, PI, 0);

  beginShape();
  // arc to start of sine wave
  vertex(bx, by + bheight/3 - 1)
  vertex(bx, by + bheight - 1);

  let a = PI;
  let r = bheight / 16;
  for (let i = bx + r/2; i < (bx + bwidth); i++) {
    y = by + bheight - r + r * sin(a);
    a += 0.045;
    // sine wave points, values above found by exploration
    vertex(i, y);
  }

  // and back to the arc
  vertex(bx + bwidth, by + bheight/3 - 1);
  endShape();
  pop();
}


function eye(ex, ey, ewidth, eheight, direction) {
  push();
  // outer
  fill(255);
  ellipse(ex, ey, ewidth, eheight);

  // inner
  fill(0);
  let iw = ewidth / 3;
  let ih = eheight / 3;
  let lr = Math.min(iw, ih);

  switch (direction) {
    case 'UP':
      ellipse(ex, ey - ih/2, iw, ih);
      break;
    case 'DOWN':
      ellipse(ex, ey + ih/2, iw, ih);
      break;
    case 'LEFT':
      ellipse(ex - iw/2, ey, iw, ih);
      break;
    case 'RIGHT':
      ellipse(ex + iw/2, ey, iw, ih);
      break;
    default:
      // center
      ellipse(ex, ey, iw, ih);
  }
  pop();
}

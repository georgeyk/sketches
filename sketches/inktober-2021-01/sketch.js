/*
Inktober 2021
Prompt: Crystal
*/
function setup() {
  angleMode(DEGREES);
  colorMode(HSB);
  createCanvas(800, 600);
  background(248, 70, 35);
  noLoop();
}

function draw() {
  translate(400, 100);
  for (let i = -30; i < 31; i = i + 30) {
    push();
    rotate(i);
    crystal(i * 10, 0);
    pop();
  }
}

function crystal(cx, cy) {
  let cw = random(50, 120);
  let ch = random(100, 250);

  beginShape(TRIANGLE_FAN);
  fill(270, 40, 95);

  // top left
  let tx = random(cx, cx + cw);
  vertex(tx, cy);
  // mid left
  let mly = random(cy + ch / 5, cy + ch / 10);
  vertex(cx, mly);
  // bottom
  vertex(cx, cy + ch);
  vertex(cx + cw, cy + ch);

  fill(259, 58, 55);

  // mid right
  let mry = random(cy + ch / 5, cy + ch / 10);
  vertex(cx + cw, mry);
  // top right
  vertex(tx, cy);
  // mid to mid
  vertex(cx, mly);
  vertex(cx + cw, mry);

  endShape();
}

/*
 Inktober 2021
 Prompt: Roof
*/
function setup() {
  createCanvas(800, 600);
  noLoop();
}

function draw() {
  // background(38, 38, 37);
  background(23, 38, 33);
  noStroke();

  push();
  fill(242, 187, 119);
  ellipse(325, 500, 50, 100);

  stroke(80);
  line(335, 410, 335, 520);
  noFill();
  arc(330, 520, 10, 10, 0, PI);

  fill(217, 85, 120);
  triangle(255, 440, 405, 440, 335, 410);
  pop();


  push();
  fill(217, 211, 180);
  ellipse(550, 400, 250, 300);

  stroke(80);
  line(570, 190, 570, 350);
  arc(565, 350, 10, 10, 0, PI);

  fill(86, 94, 140);
  fill(158, 151, 204);
  triangle(490, 225, 630, 225, 570, 190);
  pop();

  for (let i = 0; i < 1000; i++) {
    stroke(217, 211, 180, random(10, 50));
    strokeWeight(random(0.5, 1.5));

    let rx = random(800);
    let ry = random(600);
    line(rx, ry, rx, ry + random(60));
  }
}

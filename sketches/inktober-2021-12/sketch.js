/*
 Inktober 2021
 Prompt: Stuck
*/
function setup() {
  createCanvas(800, 600);
}

xoff = 0;
yoff = 0;

function draw() {
  background(29, 45, 68);

  nx = noise(xoff) * 800 / 2;
  ny = noise(yoff) * 600 / 3;

  line(0, 0, 125 + nx, 125 + ny);
  line(125 + nx, 350 + ny, 0, 600);
  line(500 + nx, 350 + ny, 800, 600);
  line(500 + nx, 125 + ny, 800, 0);

  line(500 + nx, 125 + ny, 125 + nx, 125 + ny);
  line(500 + nx, 350 + ny, 500 + nx, 125 + ny);
  line(125 + nx, 125 + ny, 125 + nx, 350 + ny);
  line(125 + nx, 350 + ny, 500 + nx, 350 + ny);

  translate(125 + nx, 350 + ny);
  fill(193, 197, 68);
  rect(150 - nx / 30, 0, 50, -100, 0, 0, 50, 50);
  fill(10);
  rect(150 + nx / 40, 0, 50, -100, 0, 0, 50, 50);
  fill(241, 211, 2);
  rect(150, 0, 50, -100, 0, 0, 50, 50);

  xoff += 0.005;
  yoff += 0.003;
}

/*
 Inktober 2021
 Prompt: Leak
*/

function setup() {
  createCanvas(800, 600);
  colorMode(HSB);
}

function draw() {
  background(204, 48, 73);

  push();
  noFill();
  strokeWeight(60);
  stroke(0, 0, 85);

  xline(200, 200, 600, 220, 0.003);
  xline(200, 400, 600, 420, 0.003);

  yline(200, 200, 120, 400, 0.005);
  yline(600, 200, 680, 400, 0.005);

  noStroke();
  fill(206, 24, 11);

  rect(200, 190, 370, 30);
  rect(250, 390, 370, 30);

  rect(190, 190, 30, 130);
  rect(590, 230, 30, 190);
  pop();
}


function xline(x1, y1, x2, y2, scale) {
  beginShape();
  vertex(x1, y1);
  for (let i = x1+1; i <= x2; i += 1) {
    let s = map(noise(i * scale), 0, 1, -1, 1);
    let y = y1 + s * (y2 - y1);

    vertex(i, y);
  }
  vertex(x2, y1);
  endShape();
}

function yline(x1, y1, x2, y2, scale) {
  beginShape();
  vertex(x1, y1);
  for (let i = y1+1; i <= y2; i += 1) {
    let s = noise(i * scale);
    let x = x1 + s * (x2 - x1);

    vertex(x, i);
  }
  vertex(x1, y2)
  endShape();
}

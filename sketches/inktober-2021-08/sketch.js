/*
 Inktober 2021
 Prompt: Watch
*/
const amplitude = 150;
let colors = [];

function setup() {
  createCanvas(800, 600);
  colors = [
    color(161, 0, 242, 95),
    color(4, 231, 98, 95),
    color(245, 183, 0, 95),
    color(220, 0, 115, 95),
    color(0, 139, 248, 95),
    color(137, 252, 0, 95),
  ];
  frameRate(2);
}

function draw() {
  background(200);
  translate(400, 300);

  strokeWeight(10);
  noFill();
  for (let i = 0; i < 100; i++) {
    stroke(random(colors));
    beginShape();
    for (let angle = 0; angle < TWO_PI; angle+= 0.5) {
      let x = cos(random(10) * angle) * amplitude;
      let y = sin(random(10) * angle) * amplitude;
      curveVertex(x, y);
    }
    endShape();
  }

  push();
  stroke(0);
  strokeWeight(90);
  ellipse(0, 0, 450, 450);

  strokeWeight(1);
  fill(200);
  textSize(32);
  let texts = ["III", "VI", "IX", "XII"];
  for (let d = 0, i = 0; d < TWO_PI; d += TWO_PI/4, i += 1) {
    text(texts[i], cos(d) * 225 - 10, sin(d) * 225 + 10);
  }
  pop();
}

/*
 Inktober 2021
 Prompt: Patch
*/
function setup() {
  createCanvas(800, 800);
  rectMode(CENTER);
  noLoop();
}

function draw() {
  background(65);
  clrs = [
    color(255, 173, 173, 95),
    color(255, 214, 165, 95),
    color(253, 255, 182, 95),
    color(202, 255, 191, 95),
    color(155, 246, 255, 95),
    color(160, 196, 255, 95),
    color(189, 178, 255, 95),
    color(255, 198, 255, 95),
    color(255, 255, 252, 95),
  ];

  noStroke();
  for (let i = 0; i < 800; i+= 20) {
    for (let j = 0; j < 800; j+= 20) {
      fill(random(clrs));
      rect(i, j, 800, 800);
    }
  }

  push();
  fill(100, 90);
  let q = 800/5;
  translate(380, -70);
  rotate(HALF_PI/2);

  rect(q, q, q);
  rect(q*3, q, q);
  rect(q, q*3, q);
  rect(q*3, q*3, q);
  pop();
}

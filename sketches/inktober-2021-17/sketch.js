/*
 Inktober 2021
 Prompt: Collide
*/

let bg;
let c1;
let c2;
let colliding = false;
let colliding_count = 0;
let base_x = 400;
let v1;
let v2;

function setup() {
  createCanvas(800, 600);
  colorMode(HSB);
  frameRate(30);

  bg = color(200, 10, 25);
  c1 = color(55, 80, 100);
  c2 = color(279, 57, 98);

  v1 = createVector(-base_x, 0);
  v2 = createVector(base_x, 0);

}

function draw() {
  background(bg);
  translate(400, 300);

  if (colliding) {
    for (let i = 0; i < 100 - colliding_count; i++) {
      strokeWeight(random(3));

      stroke(c1, random());
      let v = p5.Vector.random2D();
      v.setMag(random(100, 250));
      line(0, 0, v.x, v.y);

      stroke(c2, random());
      let u = p5.Vector.random2D();
      u.setMag(random(100, 200));
      line(0, 0, u.x, u.y);
    }
    colliding_count += 1;
  }
  else {
    strokeWeight(30);
    stroke(c1);
    line(v1.x, v1.y, v1.x - v1.mag(), v1.y);
    v1.setMag(v1.mag() - 10);

    stroke(c2);
    line(v2.x, v2.y, v2.x + v2.mag(), v2.y);
    v2.setMag(v2.mag() - 10);
  }

  if (v1.mag() == 0 && v2.mag() == 0) {
    v1 = createVector(-base_x, 0);
    v2 = createVector(base_x, 0);
    colliding = true;
    colliding_count = 0;
  }

  if (colliding_count > 100) {
    colliding = false;
    colliding_count= 0;
  }
}

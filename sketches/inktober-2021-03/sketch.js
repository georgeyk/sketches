/*
Inktober 2021
Prompt: Vessel
*/
function setup() {
  createCanvas(800, 600);
  angleMode(DEGREES);
  noLoop();
}

function draw() {
  let bg = color(224, 172, 82);
  background(bg);
  translate(400, 300);
  noFill();


  strokeWeight(3);
  stroke(220);
  for (let l = -400; l < 400; l += 50) {
    line(l, -300, l, 50);
  }

  strokeWeight(1);
  stroke(69, 123, 165)
  for (let l = 50; l < 300; l += 10) {
    line(-400, l, 400, l);
  }


  rotate(5);
  for (let a of [-9, 9, 9]) {
    rotate(a);
    stroke(bg);
    strokeWeight(30);
    arc(0, 25, 550, 150, 0, 180);
    strokeWeight(5);
    stroke(146, 95, 55);
    arc(0, 25, 550, 150, 0, 180);
  }
}

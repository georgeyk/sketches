/*
George Y. Kussumoto
EDX - Creative Coding - NYUx DMEDX6063
Week 8 - Oct/2021

Prompt:
Create a Particle System using custom Javascript Objects (ES6 class notation).
Your Particle object must contain the following properties: position, size, color, speed.
*/

// it's really cool if your machine can handle above 10k particles
const initial_particles = 1000;
const noise_step = 0.002;
const width = 800;
const height = 800;
let bg;


class Particle {
  constructor(x, y, size, colour, speed) {
    this.colour = colour;
    this.position = createVector(x, y);
    // size will drive Y-axis
    this.size = size;
    // speed will drive the X-axis
    this.speed = speed;
    // when completed is true, it means that any other movement will not be visible
    this.completed = false;
  }

  update(angle, max_x, max_y) {
    this.position.x = this.position.x + cos(angle) * this.speed;
    this.position.y = this.position.y + sin(angle) * this.size;

    if (this.position.x < 0 || this.position.x > max_x || this.position.y < 0 || this.position.y > max_y) {
      this.completed = true;
    }

  }

  show() {
    push();
    stroke(this.colour);
    point(this.position.x, this.position.y);
    pop();
  }
}


function setupParticles(width, height) {
  let particles = [];
  for (let i = 0; i < initial_particles; i++) {
    let p = new Particle(
      x=random(width),
      y=random(height),
      size=random(1, 2),
      colour=color(random(40), 80, 80, random(0.1, 0.9)),
      speed=random(1, 2)
    );

    particles.push(p);
  }
  return particles;
}

function setup() {
  createCanvas(width, height);
  colorMode(HSB);

  bg = color(210, 20, 16);
  background(bg);
  frameRate(30);

  particles = setupParticles(width, height);
}


function draw() {
  for (let p of particles) {
    if (!p.completed) {
      p.show();

      // visible in the next iteration
      let angle = map(noise(p.position.x * noise_step, p.position.y * noise_step), 0, 1, -PI, PI);
      p.update(angle, width, height);
    }
    else {
      // remove particle
      let idx = particles.indexOf(p);
      particles.splice(idx, 1);
    }
  }

  // reset
  if (particles.length == 0) {
    background(bg);
    particles = setupParticles(width, height);
  }
}

/*
 Inktober 2021
 Prompt: Fuzzy
*/
function setup() {
  createCanvas(800, 600);
  noLoop();
}

function draw() {
  background(65);
  let clrs = [
    color(254, 74, 73),
    color(254, 215, 102),
    color(0, 159, 183),
    color(244, 244, 248),
  ];
  let ly = 20;
  let hy = 580;

  for (let y = 100; y <= 500; y += 50) {
    stroke(random(clrs));

    for (let x = 20; x < 780; x += 3) {
      let h = map(noise(x * 0.03, y), 0, 1, 0, 100);
      line(x, y - h, x, y + h);
    }
  }
}

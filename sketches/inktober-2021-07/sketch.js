/*
 Inktober 2021
 Prompt: Fan
*/
function setup() {
  createCanvas(800, 600);
  angleMode(DEGREES);
  rectMode(CENTER);
  noLoop();
}


function draw() {
  background( 233, 226, 199);
  translate(400, 350);

  push();
  strokeWeight(3);
  rotate(45);
  fill(212, 226, 212);
  rect(0, 0, 300, 300, 150, 0);
  fill(223, 120, 97);
  rect(75, 75, 150, 150, 150, 0);
  // rect(100, 100, 100, 100, 150, 0);
  // fill(252, 248, 232);
  fill(252, 248, 232);
  rect(125, 125, 50, 50, 150, 0);
  pop();

  for (let i = 0; i < 10; i++) {
    line(0, 210, 50 * i, -15* random(5) * i)
    line(0, 210, -50 * i, -15* random(5) * i)
  }
}


function mouseClicked() {
  redraw();
}

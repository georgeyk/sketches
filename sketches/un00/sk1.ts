let world = {
  width: 400,
  height: 400,
  gap: 5,
  txts: ["¯\\_(ツ)_/¯", "( ͡° ͜ʖ ͡°)", "(°ロ°)☝",]
}


function setup() {
  createCanvas(world.width, world.height);
}

function draw() {
  background('#1d3557');
  fill('#457b9d');
  blocks();

  clip(blocks);
  fill('#e63946')

  textAlign(CENTER, CENTER);
  textStyle(BOLD);
  textSize(100);
  text(random(world.txts), world.width / 2, world.height / 2);

  noLoop();
}

function blocks() {
  noStroke();
  height = 10;
  round_value = 6;
  for (let y = 5; y < world.height - world.gap; y += world.gap) {
    for (let x = random(5, 15); x < world.width - world.gap; x += world.gap) {
      w = constrain(random(30, 90), world.gap, world.width - x - world.gap);
      if (w < 10) { continue; }

      rect(x, y, w, height, round_value);
      x += w;
    }
    y += height;
  }
}

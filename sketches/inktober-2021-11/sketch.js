/*
 Inktober 2021
 Prompt: Sour
*/
function setup() {
  createCanvas(800, 600);
  colorMode(HSB);
  noLoop();
}

function draw() {
  translate(400, 265);
  let gr = color(70, 100, 80);
  let yl = color(49, 63, 98);
  let ow = color(49, 11, 99);
  background(ow);

  rotate(HALF_PI/4);

  fill(gr);
  for (r = 250; r > 0; r -= 50) {
    if (r < 250) {
      fill(yl);
    }
    for (i = 0; i < PI + HALF_PI; i += (PI+HALF_PI)/r*12) {
        x = cos(i) * r;
        y = sin(i) * r;
        rect(x, y, 50, 50, 10, 50, 10, 50);

        for(k=0; k < 20;k++) {
          point(random(x+40, x+20), random(y+40, y+20));
        }
      }
  }

  rect(0, 0, 50, 50, 10, 50, 10, 50);
  point(random(40, 20), random(40, 20));
  point(random(40, 20), random(40, 20));
  point(random(40, 20), random(40, 20));
}

/*
George Y. Kussumoto
EDX - Creative Coding - NYUx DMEDX6063
Week 12 - Nov/2021

Prompt:
Create an image sprite animation using p5.play. The animation must respond to keyboard and mouse interaction.

Assets by Kenney: https://www.kenney.nl/assets
*/

let ship;
let acc_on = false;
let sframe = 0;
let max_meteors = 30;
let meteors;

function preload() {
  ship = createSprite(400, 300, 128, 128);
  ship.addImage(loadImage('assets/ship.png'));
  let anim = ship.addAnimation('accelerate', 'assets/ship_acc_01.png', 'assets/ship_acc_08.png')
  anim.frameDelay = 8;

  meteors = new Group();
}


function createMeteor() {
  let pick = random(['small', 'large']);
  let meteor = createSprite(random(800), -50);

  if (pick == 'small') {
    meteor.addImage(loadImage('assets/meteor_small.png'));
    meteor.setCollider('circle', 0, 0, 16);
  }
  else {
    meteor.addImage(loadImage('assets/meteor_large.png'));
    meteor.setCollider('circle', 0, 0, 24);
    meteor.mass = 2;
  }

  meteor.rotationSpeed = random(1, 2);
  meteor.setSpeed(random(1, 2), random(360))

  return meteor;
}

function checkMeteors() {
  for (let m of meteors) {
    if (m.position.x < 0 || m.position.x > 800 || m.position.y > 600) {
      m.remove();
      meteors.remove(m);
    }
  }

  if (meteors.size() < max_meteors) {
    meteors.add(createMeteor());
  }
}


function setup() {
  createCanvas(800, 600);
}


function draw() {
  background(100);

  // avoid erratic behaviors when the mouse is not moving
  if ((ship.position.x  >= mouseX - 10 || ship.position.x <= mouseX + 10) &&
      (ship.position.y >= mouseY - 10 || ship.position.y <= mouseY + 10)) {
      ship.setSpeed(0);
  }

  // acceleration handling
  if (keyWentDown('space')) {
    acc_on = true;
    sframe = frameCount;
  }
  if (keyWentUp('space')) {
    acc_on = false;
  }
  // interrupt acceleration, otherwise it runs indefinitely
  if (frameCount - sframe > 30) {
    acc_on = false;
  }

  ship.attractionPoint(1, mouseX, mouseY);

  if (acc_on) {
    // always speed "up"
    ship.setSpeed(5, -90);
    ship.changeAnimation('accelerate');
  }
  else {
    ship.setSpeed(1);
    ship.limitSpeed(10);
    ship.changeAnimation('normal');
  }

  checkMeteors();
  meteors.bounce(meteors);
  ship.displace(meteors);
  drawSprites();
}

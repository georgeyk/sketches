/*
Inktober 2021
Prompt: Raven
*/
let bg;
function setup() {
  createCanvas(800, 600);
  bg = color(0, 100, 102);
  background(bg);
  noLoop();
}

function draw() {
  translate(400, 300);
  let p = color(39, 38, 64);
  strokeWeight(3);

  // wings
  fill(p);
  arc(0, -100, 400, 150, 0, PI, CHORD);
  rect(-75/2, -150, 75, 150, 20, 20, 0, 0);

  // eyes
  push();
  strokeWeight(1);
  fill(77, 25, 77);
  ellipse(-20, -120, 25, 40);
  ellipse(20, -120, 25, 40);
  fill(220);
  ellipse(-20, -120, 20, 20);
  ellipse(20, -120, 20, 20);
  fill(25);
  ellipse(-20, -120, 10, 10);
  ellipse(20, -120, 10, 10);
  pop();

  // beak
  push();
  fill(113, 78, 10);
  translate(0, -105);
  rotate(HALF_PI/2);
  rect(0, 0, 30, 30, 20, 0, 0, 0);
  pop();

  // bottom
  fill(p);
  rect(-25, 0, 50, 150);
  rect(-50, 150, 100, 50, 20, 20, 0, 0);

  // detail lines

  stroke(77, 25, 77);
  strokeWeight(5);
  // mid
  line(-20, -40, 20, -40);
  line(-20, -30, 20, -30);
  line(-20, -20, 20, -20);

  // wing
  line(-150, -75, -150, -90);
  line(-125, -60, -125, -90);
  line(-100, -50, -100, -90);
  line(-75, -45, -75, -90);

  line(150, -75, 150, -90);
  line(125, -60, 125, -90);
  line(100, -50, 100, -90);
  line(75, -45, 75, -90);

  // bottom
  line(-10, 25, -10, 120);
  line(10, 25, 10, 120);
  line(-40, 170, 40, 170);
  line(-40, 185, 40, 185);
}

/*
 Inktober 2021
 Prompt: Pick
*/
let pheight = 25;
let pwidth= 15;

function setup() {
  createCanvas(800, 600);
  noLoop();
}

function draw() {
  background(241, 250, 238);
  translate(400, 300);

  let angle = 0;
  for (let i = 0; i < TWO_PI; i+=0.009){
    rotate(i);
    pill(random(800-pwidth), random(600-pheight*2));
  }

}


function pill(px, py) {
  push();
  fill(230, 57, 70);
  rect(px, py, pwidth, pheight, 25, 25, 0, 0);
  fill(69, 123, 157);
  rect(px, py + pheight, pwidth, pheight, 0, 0, 25, 25);
  pop();
}

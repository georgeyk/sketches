/*
 Inktober 2021
 Prompt: Moon
*/
function setup() {
  createCanvas(800, 600);
  background(78, 91, 128);
  noLoop();
}

function draw() {
  translate(400, 300);

  for (let i = 0; i < 10000; i++) {
    let a = random(TWO_PI);
    let r = 250 * Math.sqrt(random());

    strokeWeight(random(3));
    stroke(192, random(255));
    point(cos(a) * r, sin(a) * r);
  }

  noStroke();
  fill(78, 91, 128);
  ellipse(50 + random(-150, 150), 50 + random(-150, 150), 500, 500);
}

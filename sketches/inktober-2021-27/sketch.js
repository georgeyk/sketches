/*
 Inktober 2021
 Prompt: Spark
*/
function setup() {
  createCanvas(800, 600);
  frameRate(5);
}

function draw() {
  background(5);
  translate(400, 300);

  strokeWeight(random(1, 2));
  spark(0, 0, random(300), random(5, 50));
}


function spark(cx, cy, r, a) {
  let clrs = [
    color(235, 242, 250),
    color(52, 209, 255),
    color(237, 174, 73),

  ];
  push();
  translate(cx, cy);
  noFill();
  for (let i = 0; i < a; i++) {
    let angle = random(TWO_PI);
    let x1 = cos(angle) * random(1, r);
    let y1 = sin(angle) * random(1, r);

    stroke(random(clrs));
    line(0, 0, x1, y1)

    if (random() > 0.5) {
      strokeWeight(0.75);
      ellipse(x1 + random(-50, 50), y1-random(-50, 50), random(10));
    }

    if (random() > 0.9) {
      strokeWeight(0.2);
      ellipse(x1, y1, random(r));
    }

    if (random() > 0.97) {
      strokeWeight(0.2);
      rect(x1, y1, random(r));
    }

   if (random() > 0.99) {
      strokeWeight(0.2);
      triangle(x1, y1, x1 + random(r), y1 + random(r), x1 + random(r/2), y1 + random(r/2));
    }

  }
  pop();
}

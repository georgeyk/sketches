/*
George Y. Kussumoto
EDX - Creative Coding - NYUx DMEDX6063
Week 10 - Oct/2021

Prompt:
Choose one of the following poems, and replace all nouns and adjectives in the text with random nouns and adjectives.
If there is rhymescheme in the poem, try changing it to match your new words.

Selected:
New Hampshire
Stopping by Woods on a Snowy Evening
https://en.wikisource.org/wiki/New_Hampshire_(Frost)/Stopping_by_Woods_on_a_Snowy_Evening
*/


let title = "STOPPING BY WOODS ON A SNOWY EVENING";
let poem = [
  "Whose woods these are I think I know.",
  "His house is in the village though;",
  "He will not see me stopping here",
  "To watch his woods fill up with snow.",
  "",
  "My little horse must think it queer",
  "To stop without a farmhouse near",
  "Between the woods and frozen lake",
  "The darkest evening of the year.",
  "",
  "He gives his harness bells a shake",
  "To ask if there is some mistake.",
  "The only other sound's the sweep",
  "Of easy wind and downy flake.",
  "",
  "The woods are lovely, dark and deep.",
  "But I have promises to keep,",
  "And miles to go before I sleep,",
  "And miles to go before I sleep.",
];


function remix_poem() {
  let remixed = [];
  let blocklist = [];

  for (let line of poem) {
    let tokens = RiTa.tokenize(line);
    let outcome = [];

    for (let i = 0; i < tokens.length; i++) {
      let token = tokens[i];

      // keep punctuation right after the tokeninzation
      if (token.length <= 1  && RiTa.isPunct(token)) {
        let v = outcome.pop();
        v += token;
        outcome.push(v);
        continue;
      }

      // detect if it's the last word of the line, if so, we want to keep the rhymescheme
      let keepRhyme = false;
      if (i == tokens.length - 2 && RiTa.isPunct(tokens[i+1])) {
        keepRhyme = true;
      }
      if (i == tokens.length - 1 && RiTa.isPunct(token) == false) {
        keepRhyme = true;
      }

      let a = RiTa.analyze(token, {silent:true});
      let next_word;

      // nn = nouns ; jj = adjectives
      if (a.pos.startsWith("nn") || a.pos.startsWith("jj")) {
        let phonemes = a.syllables.split("-").length - 1;

        if (keepRhyme) {
          next_word = replaceRhyme(token, a.pos, phonemes, blocklist);
        }
        else {
          next_word = replaceWord(a.pos, phonemes, blocklist);
        }

        outcome.push(next_word);
        // add to the blocklist to avoid repeat the same replacement
        blocklist.push(next_word);
      }
      else {
        // Not a noun or adjective, keep it "as-is"
        outcome.push(token);
      }
    }

    remixed.push(outcome.join(" "));
  }

  return remixed;
}


function replaceWord(pos, phonemes, blocklist) {
  let candidate;
  try {
    candidate = RiTa.randomWord({pos: pos, numSyllables: phonemes});
  }
  catch (err) {
    // try again, but less restrictive (less syllables -> more results)
    return replaceWord(pos, phonemes-1, blocklist);
  }

  if (candidate && blocklist.indexOf(candidate) == -1) {
    return candidate;
  }
}


function replaceRhyme(word, pos, phonemes, blocklist) {
  let candidates = RiTa.rhymes(word, {pos: pos, numSyllables: phonemes});

  for (let i = 0; i < candidates.length; i++) {
    let candidate = random(candidates);
    if (blocklist.indexOf(candidate) == -1) {
      return candidate;
    }
  }
  // same idea from replaceWord, less syllables -> more results
  return replaceRhyme(word, pos, phonemes-1, blocklist);
}


// p5 entrypoints


function setup() {
  createCanvas(windowWidth, windowHeight);
  noLoop();
}


function draw() {
  background(254, 252, 230);

  // soft sprinkles
  push();
  for (let i = 0; i < 1000; i++) {
    stroke(random([color(13, 59, 102, 95), color(87, 184, 255, 95)]))
    strokeWeight(random(3));

    point(random(windowWidth), random(windowHeight));
  }
  pop();

  fill(109, 76, 61);
  textSize(22);
  textStyle(BOLD);
  text(title, windowWidth/5, 100, windowWidth);

  let y = 150;

  textSize(16);
  textStyle(NORMAL);

  let remixed = remix_poem();

  for (let line of remixed) {
      text(line, windowWidth/5, y, windowWidth);
      y += 30;
  }
}


function keyPressed() {
  redraw();
}

/*
George Y. Kussumoto
EDX - Creative Coding - NYUx DMEDX6063
Week 2 - Aug-Sept/2021

Prompt:
Choose 1 from the following 3 artworks, and recreate the work using p5.js.
You must credit the original artist in your code/documentation with artist name, title of work, and date.
In your code, you must use variables (including p5’s built-in width and height variables) as well as a conditional expression.

Notes:

Recreation of:

Concreto, Alfredo Volpi, December 1950
https://artsandculture.google.com/asset/concreto/XQGNUoKY-Ec7HA
*/

/*
Imagine that we have a grid where each tile is a rectangle.
On each tile, we can choose to:
0: do not paint it
1: paint green from left most side to the rectangle diagonal
2: paint green from right most side to the rectangle diagonal
3: the same as 1, but yellow
The matrix below represents the tiles of "Concreto".
*/
const concreto = [
  [0, 1, 0, 0],
  [2, 0, 3, 3],
  [0, 1, 3, 3],
  [2, 0, 3, 3],
  [0, 1, 3, 3],
  [2, 0, 3, 3],
  [0, 3, 3, 3],
];

const max_rows = concreto.length;
const max_cols = concreto[0].length;
// we want to keep this proportion to be drawing consistent across distinct scales.
const min_col = 40;
const min_row = 60;
var scale_factor = 1;

function setup() {
  createCanvas(windowWidth, windowHeight);
  // the maximum that we can scale the drawing is the minimum
  // that we can fit on window width or height available
  let scale_w = Math.floor(windowWidth / (max_cols * min_col));
  let scale_h = Math.floor(windowHeight / (max_rows * min_row));
  scale_factor = Math.min(scale_w, scale_h);
}


function draw() {
  background(242);
  noStroke();
  var green = color(12, 89, 89);
  var yellow = color(217, 205, 145);

  // find the amount that we need to shift X and Y coordinates in order to draw
  // at the center of the canvas
  var row_step = min_row * scale_factor;
  var col_step = min_col * scale_factor;
  var move_x = (windowWidth - (col_step * max_cols)) / 2;
  var move_y = (windowHeight - (row_step * max_rows)) / 2;
  translate(move_x, move_y);

  // draw the matrix using our rules
  for (let row = 0; row < max_rows; row++) {
    for (let col = 0; col < max_cols; col++) {
      if (concreto[row][col] == 1) {
        draw_half_rect(row, col, paint_left=true, green);
      }
      else if (concreto[row][col] == 2) {
        draw_half_rect(row, col, paint_left=false, green);
      }
      else if (concreto[row][col] == 3) {
        draw_half_rect(row, col, paint_left=true, yellow);
      }
    }
  }

  noLoop();
}

function draw_half_rect(row, col, paint_left=true, paint_color) {
  // translate from the matrix indexes to canvas position
  var row_step = min_row * scale_factor;
  var col_step = min_col * scale_factor;
  var vertex_row = row * row_step;
  var vertex_col = col * col_step;

  push();
  fill(paint_color);
  beginShape();
  vertex(vertex_col, vertex_row);
  vertex(vertex_col + col_step, vertex_row);
  // at this point we have the top vertices of the rectangle set.
  // depending on "paint_left" value, we set the downwards Y coordinate
  // (y grows from top to bottom) to get the desired diagonal line.
  if (paint_left) {
    vertex(vertex_col, vertex_row + row_step);
  }
  else {
    vertex(vertex_col + col_step, vertex_row + row_step);
  }
  endShape();
  pop();
}

/*
 Inktober 2021
 Prompt: Connect
*/
function setup() {
  createCanvas(800, 600);
  noLoop();
}

function draw() {
  background(60, 110, 113);

  for (let i = 0; i < 42; i++) {
    let r = random(50, 350);
    wifi(random(800 - i) , random(600 + i), r);
  }
}


function wifi(x, y, r) {
  let clrs = [
    color(4, 150, 255),
    color(217),
    color(65),
  ]
  let c = random(clrs);

  stroke(c);
  strokeWeight(random(r/22));
  noFill();

  arc(x, y, r/2, r/2, PI + HALF_PI/2, -HALF_PI/2);
  arc(x, y, r/3, r/3, PI + HALF_PI/2, -HALF_PI/2);
  arc(x, y, r/5, r/5, PI + HALF_PI/2, -HALF_PI/2);

  if (random() > 0.5) {
    noFill();
  }
  else {
    fill(c);
  }
  ellipse(x, y, r/14);

  if (random() < 0.1) {
    line(x - r/6, y + r/13, x + r/6, y - r/3);
  }
}

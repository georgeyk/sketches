/*
 Inktober 2021
 Prompt: Sprout
*/

let bg;
let gr;
let br;
let rd;


function setup() {
  createCanvas(800, 600);
  bg = color(237, 224, 212);
  gr = color(101, 109, 74);
  br = color(57, 42, 22);
  rd = color(137, 13, 4);
  background(bg);
  noLoop();
}

function draw() {
  let angle = 0;
  let pops = [];
  for (let i = 100; i < 800; i+=100) {
    let m = random(1, 30);

    strokeWeight(random(1, 6));

    for (let y = 0; y < 600; y += 1) {
      if (y < 150) {
        stroke(lerpColor(bg, br, y/150));
      }
      else {
        stroke(br);
      }


      let x = i + cos(angle) * m;
      point(x, y);

      if (y > 0 && y % 100 == 0) {
        pops.push([x, y]);
      }

      pops.forEach((e) => {
        push();
        noStroke();
        fill(random([gr, gr, gr, rd]));

        ellipse(e[0] + random(-10, 10), e[1] + random(-50, 50), 25, 25);
        pop();
      })

      pops = [];
      angle += 0.015;
    }

  }

}

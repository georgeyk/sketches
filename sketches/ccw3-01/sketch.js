/*
George Y. Kussumoto
EDX - Creative Coding - NYUx DMEDX6063
Week 3 - Sept/2021

Prompt:
Create a sketch that uses beginShape() and endShape() to draw nested/repeated shapes that form animated Moire patterns.
Each sketch must contain vertices, and your sketch should utilize the following p5.js functions:

- beginShape()
- endShape()
- vertex()
- for loop

Moire patterns: https://en.wikipedia.org/wiki/Moir%C3%A9_pattern
*/

const max_x = 600;
const max_y = 600;
const flame_scale = 100;
const flame_width = 1 * flame_scale;
const flame_height = 3 * flame_scale;


function setup() {
  createCanvas(max_x, max_y);
  frameRate(30);
  angleMode(DEGREES);
}


function draw() {
  background(2, 48, 71);
  strokeWeight(8);
  noFill();

  // flame color palette
  yellow = color(247, 178, 103, 150);
  orange = color(244, 132, 95, 150);
  redc = color(242, 92, 84, 150);

  // position elements at the center
  py = max_y / 6;
  px = max_x/2 - py/2;

  // found by experimentation and enjoyed the little pattern created
  // changing to 50 or 75 is also cool to look at
  k = 25 * sin(frameCount);

  // static
  candle_flame(px, py, flame_width, flame_height, redc);
  candle_body(px, py + 200);
  // horizontal movement
  candle_flame(px + k, py, flame_width, flame_height, orange);
  candle_flame(px - k, py, flame_width, flame_height, orange);
  // vertical movement
  candle_flame(px, py + k, flame_width, flame_height, yellow);
  candle_flame(px, py - k, flame_width, flame_height, yellow);
  // a simpler Moire effect is also visibile without the vertical movement,
  // but the vertical animation adds to a more interesting complexity
}


function candle_flame(x, y, width, height, flame_color) {
  // complex shape by joining a triangle and half-circle
  push();
  let gap = 25;
  stroke(flame_color);
  translate(x, y);

  beginShape(POINTS);
  // a triangle shape like /\
  for (let i = 0; i < height/3; i += gap) {
    let x1 = width/2 - i/2;
    let x2 = width/2 + i/2;
    // as we step in Y, we add (x, y) and (-x, y)
    vertex(x1, i);
    vertex(x2, i);
  }

  // half bottom circle shape
  let radius = height/6;
  for (let angle = 0; angle <= 180; angle += 30) {
    let cx = radius * cos(angle);
    let cy = radius * sin(angle);
    // the arc is at the tip of the triangle, so we align at the bottom
    vertex(cx + width/2, cy + height/3);
  }
  // center "dot"
  vertex(width/2, height/3);
  endShape();
  pop();
}


function candle_body(x, y, candle_height=250, gap=25) {
  // a rectangle made of dots, fading out with height
  push();
  let columns = 5;
  let cream = color(234, 226, 183);
  stroke(cream);

  beginShape(POINTS);
  for (let i = y; i < y + candle_height; i += gap) {
    // fade-out
    alpha_color = color(
      red(cream),
      green(cream),
      blue(cream),
      255 - (i - y),
    );
    stroke(alpha_color);

    for (let j = 0; j < columns; j++) {
      vertex(x + j * gap, i);
    }
  }
  endShape();
  pop();
}

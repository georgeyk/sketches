/*
Inktober 2021
Prompt: Spirit
*/
let angle = 0;
let amplitude = 200;

function setup() {
  createCanvas(800, 600);
  background(189, 212, 191);
  rectMode(CENTER);
  frameRate(20);
}

function draw() {
  translate(400, 500);
  rotate(HALF_PI);

  x = tan(angle) * sin(2 * angle) * amplitude;
  y = cos(angle) * sin(4 * angle) * amplitude;

  noStroke();

  fill(111, 132, 215, 40);
  rect(-x, y/2, 20, 20, 50);

  fill(255, 207, 206, 60);
  rect(-x, -y, 20, 20, 50);

  angle += 0.01;
}

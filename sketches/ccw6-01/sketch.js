/*
George Y. Kussumoto
EDX - Creative Coding - NYUx DMEDX6063
Week 6 - Sept/2021

Prompt:
Using the transformation functions (translate(x,y), rotate(angle), and/or scale(x,y)) create a sketch with animated, spiralling shapes.
Your sketch must incorporate a color palette of interpolated colors using lerpColor().
To successfully complete this assignment, you must use at least two of the three transformation functions.
*/

var angle = 0;
var angle_inc = 5;
var cx;
var cy;
var max_n_rotation;
var n_rotation;
var start_color;
var end_color;
var current_color;

function setup() {
  angleMode(DEGREES);
  colorMode(HSB);
  createCanvas(windowWidth, windowHeight);
  background(63, 22, 95);

  cx = windowWidth / 2;
  cy = windowHeight / 2;
  max_n_rotation = cy/2;
  n_rotation = max_n_rotation;
  start_color = color(167, 99, 76);
  end_color = color(197, 96, 55);
  current_color = end_color;

  frameRate(20);
}


function draw() {
  translate(cx, cy);
  rotate(angle);
  // scale(1.5*sin(angle), cos(angle));
  // ^ a gift for those who read code =)

  fill(current_color);

  // we use n_rotation to "stretch" the triangle from the center of the canvas
  // you can think of it as similar to a "radius" in a circle
  triangle(-20 - n_rotation, 20 + n_rotation, 20 + n_rotation, 20 + n_rotation, 0, -20);

  // circular increment of the angle and "radius" (n_increment)
  // it resets at each full rotation, so it creates an internal layer
  // but also a "external" layer that is only visible over longer observations
  if (angle >= 359) {
    angle = 0;

    if (n_rotation <= 1) {
      n_rotation = max_n_rotation;
      // different shapes
      angle_inc = random(5, 55);
    }
    else {
      n_rotation -= 20;
    }

    // "lighter" as close it gets to the center
    current_color = lerpColor(start_color, end_color, n_rotation/max_n_rotation);
  }
  else {
    // some shapes looks more spirals than others ...
    angle += angle_inc;
  }
}

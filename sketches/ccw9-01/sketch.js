/*
George Y. Kussumoto
EDX - Creative Coding - NYUx DMEDX6063
Week 9 - Oct/2021

Prompt:
Create a user interface for your canvas sketch using the DOM library.
The interface should control the following parameters in your sketch: size, color, animationSpeed, isAnimated.
Think about how you might trigger or change these parameters with buttons, sliders, hover interaction, etc.
*/


let size;
let colour;
let animationSpeed;
let isAnimated;
let stroke_color;
const canvas = 800;


function setupControls() {
  let controls = createDiv();
  controls.id("controls");
  controls.style("width:150px;margin:15px;");
  let p = createP("Controls");
  p.parent(controls);

  isAnimated = createCheckbox('Animated', true);
  isAnimated.changed(isAnimatedCallback);
  isAnimated.parent(controls);

  let size_span = createSpan("Size:");
  size = createSlider(20, 200, 40, 10);
  size.changed(redrawChanged);
  size.parent(size_span);
  size_span.parent(controls);

  let speed_span = createSpan("Speed:");
  animationSpeed = createSlider(1, 10, 1, 1);
  animationSpeed.changed(redrawChanged);
  animationSpeed.parent(speed_span);
  speed_span.parent(controls);

  let colour_span = createSpan("Color:");
  colour = createColorPicker("#555B6E");
  colour.changed(redrawChanged);
  colour.parent(colour_span);
  colour_span.parent(controls);
}


function isAnimatedCallback() {
  if (this.checked()) {
    loop()
  }
  else {
    noLoop();
  }
}


function redrawChanged() {
  // when looping this is automatic
  if (!isLooping()) {
    redraw();
  }
}


function draw_tile(x, y, tsize) {
  let k = random([0, HALF_PI]);
  rotate(k);

  // easier to understand if you use "rect(x, y, s)"
  arc(x - tsize/2, y - tsize/2, tsize, tsize, 0, HALF_PI);
  arc(x + tsize/2, y + tsize/2, tsize, tsize, PI, -HALF_PI);
}


/*
 * p5 entrypoints
 */


function setup() {
  createCanvas(canvas, canvas);
  rectMode(CENTER);
  stroke_color = color(245);
  setupControls();
  noFill();
}

function draw() {
  frameRate(animationSpeed.value());
  background(colour.color());
  stroke(stroke_color);

  let size_input = size.value();
  let tiles = Math.round(canvas / size_input);

  if (tiles < 20) {
    strokeWeight(4);
  }
  else {
    strokeWeight(1);
  }

  for (let i = 1; i < tiles; i++) {
    for (let j = 1; j < tiles; j++) {
      push();
      translate(i * size_input, j * size_input);
      draw_tile(0, 0, size_input);
      pop();
    }
  }
}

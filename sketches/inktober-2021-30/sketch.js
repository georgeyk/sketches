/*
 Inktober 2021
 Prompt: Slither
 */


let snake = [];
let target;

function setup() {
  createCanvas(800, 600);
  frameRate(30);
  snake.push(new snake_tile(100, 100, 10));
  target = createVector(random(800-10), random(600-10));

}

function draw() {
  background(65);

  noStroke();
  fill(216, 17, 89);
  rect(target.x, target.y, 20);

  for (let t = 0; t < snake.length; t++) {
    s = snake[t];

    if (s.ttl == 0) {
      snake.splice(t, 1);
    }
    else {
      s.update();
      s.show();
    }
  }

  let last = snake.at(-1);
  if (last.pos.dist(target) > 10) {
    let next = find_next(last, target);
    snake.push(new snake_tile(next.x, next.y, 10));
  }
  else {
    target = createVector(random(800-10), random(600-10));
  }
}


function find_next(tile, target) {
  let np = createVector();
  if (tile.pos.x >= 0 && tile.pos.x <= 800) {
    if (tile.pos.x >= target.x) {
      np.x = tile.pos.x - 10;
    }
    else {
      np.x = tile.pos.x + 10;
    }
  }

  if (tile.pos.y >= 0 && tile.pos.y <= 600) {
    if (tile.pos.y >= target.y) {
      np.y = tile.pos.y - 10;
    }
    else {
      np.y = tile.pos.y + 10;
    }
  }

  return np;
}

class snake_tile {
  constructor(x, y, s) {
    this.pos = createVector(x, y);
    this.size = s;
    this.ttl = 30;
    this.c = color(255, 255, 63);
  }

  update() {
    this.ttl = Math.max(this.ttl -1, 0);
  }

  show() {
    let life = map(this.ttl, 0, 30, 0, 1);
    let nc = lerpColor(color(0, 0), this.c, life);
    noStroke();
    fill(nc);
    ellipse(this.pos.x, this.pos.y, this.size);
    ellipse(this.pos.x+10, this.pos.y, this.size);
    ellipse(this.pos.x-10, this.pos.y, this.size);
    ellipse(this.pos.x, this.pos.y+10, this.size);
    ellipse(this.pos.x, this.pos.y-10, this.size);
  }
}

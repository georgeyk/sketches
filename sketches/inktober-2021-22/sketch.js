/*
 Inktober 2021
 Prompt: Open
*/
function setup() {
  createCanvas(800, 600);
  strokeJoin(ROUND);
  strokeWeight(12);
}

let angle = 0;
function draw() {
  background(101, 1, 12);

  x = cos(1*angle) * 20;
  y = sin(2*angle) * 70;

  fill(254, 223, 212);
  triangle(400, 400, 200+x*3, 150+y, 550+x*3, 150+y);
  fill(242, 148, 121);
  triangle(400, 400, 200-x*3, 150-y, 550-x*3, 150-y);

  line(400, 400, 400, 600);

  angle += 0.01;
}

/*
 Inktober 2021
 Prompt: Crispy
*/
function setup() {
  createCanvas(800, 600);
  noLoop();
}

function draw() {
  background(238, 69, 56);

  translate(600, 50);
  rotate(HALF_PI);
  for (let h = 100; h < 500; h += 50) {
    crispy(random(800), h, 800, random(15, 40));
  }
}


function crispy(x, y, w, h) {
  let clrs = [
    color(255, 237, 62),
    color(248, 209, 39),
    color(238, 186, 10),
  ];
  let angle = 0;

  push();
  strokeCap(SQUARE);
  strokeWeight(random(1, 4));

  beginShape();
  fill(random(clrs));

  for (let i = 0; i <= w; i+=10) {
    let j = round(cos(angle) * h/2);
    vertex(x + i, y + j);
    angle += HALF_PI;
  }

  angle = 0;
  for (let i = w; i > 0; i-=10) {
    let j = round(cos(angle) * h/2);
    vertex(x + i, y + h + j);
    angle += HALF_PI;
  }
  endShape(CLOSE);
  pop();
}

/*
George Y. Kussumoto
EDX - Creative Coding - NYUx DMEDX6063
Week 1 - Aug/2021

Prompt:
Wall Drawing # 915 (1999): Arcs, circle, and irregular bands.

Notes:
I'm trying to limit the code to what was exposed in the course at the time (weeks 1-3).
Hopefully, other students can benefit from this approach if they happen to read the code.

It remind me the Copacabana's sidewalk. Doesn't it? Happy accidents.
I'm also using "ccw" tag to group the sketches made in the course period.
*/

// 16:9 proportion
const max_x = 640;
const max_y = 360;
// helpers to a imaginary a 5x5 grid
const y_step = max_y / 5;
const x_step = max_x / 5;
const half_y_step = y_step / 2;
const half_x_step = x_step / 2;

function setup() {
  createCanvas(max_x, max_y);
  background(50);
}

function draw() {
  noStroke();
  fill(245);
  blendMode(DIFFERENCE);

  for (let y = half_y_step; y <= max_y; y += y_step) {
    if (y == max_y / 2) {
      // mid row
      draw_circles(y);
    } else {
      // after mid row we "flip" arc direction to create symmetry
      draw_arcs(y, flip = y > max_y / 2);
    }
  }

  noLoop();
}

function draw_arcs(at_y, flip = false) {
  for (let i = 0; i <= max_x; i += x_step) {
    let arc_start = 0;
    let arc_end = PI;

    if (flip) {
      arc_start = arc_end;
      arc_end = 0;
    }

    // Draw "half-moons" using arcs. Added a random component in height to create the
    // irregular bands, so we have a pattern that is slightly off, but not too messy.

    // sad half-moons (if not flipped)
    arc(i, at_y, half_x_step, y_step - random(half_y_step), arc_start, arc_end);
    arc(i, at_y, half_x_step, y_step + random(half_y_step), arc_start, arc_end);

    // happy half-moons (if not flipped)
    arc(i + half_x_step, at_y, half_x_step, y_step - random(half_y_step), arc_end, arc_start);
    // reduce amplitude to stay inside the canvas (when flipped)
    arc(i + half_x_step, at_y, half_x_step, y_step - random(half_y_step / 4), arc_end, arc_start);
  }
}

function draw_circles(at_y) {
  // the circle requirement and also adding a little variety by randomizing circle radius
  for (let i = half_x_step; i < max_x; i += x_step) {
    r = half_y_step + random(half_y_step)
    ellipse(i, at_y, r, r);
  }
}

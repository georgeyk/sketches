/*
 Inktober 2021
 Prompt: Compass
*/

let angle = 0;

function setup() {
  createCanvas(800, 600);
}

function draw() {
  background(246, 244, 210);

  let a = map(noise(angle), 0, 1, 0, TWO_PI)

  for (let i = 250; i < 700; i+=150) {
    for (let j = 150; j < 600; j+=150){
      let k = i / 100;
      compass(i, j, 30, PI/k + a);
    }
  }

  angle += 0.003;
}

function compass(cx, cy, size, angle) {
  push();
  noStroke();

  translate(cx, cy);
  rotate(angle);

  fill(41, 50, 65);
  ellipse(0, 0, size * 4);

  // north
  fill(210, 108, 77);
  triangle(0, 0, -size, 0, 0, -size * 2);
  fill(250, 108, 77);
  triangle(0, 0, size, 0, 0, -size * 2);

  // south
  fill(152, 193, 250);
  triangle(0, 0, -size, 0, 0, size * 2);
  fill(152, 193, 210);
  triangle(0, 0, size, 0, 0, size *2);
  pop();
}

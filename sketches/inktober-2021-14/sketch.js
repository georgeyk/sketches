/*
 Inktober-2021
 Prompt: Tick
*/
function setup() {
  createCanvas(800, 600);
  rectMode(CENTER);
  noLoop();
}

function draw() {
  background(242, 232, 207);
  let rcolor = color(188, 71, 73);
  let gcolor = color(56, 102, 65);

  noStroke();
  for (let x = 40; x < 780; x += 20) {
    let y = map(noise(x * 0.05), 0, 1, 100, 500);
    let h = random(25, 100);

    if (y < 300) {
      stroke(gcolor);
      fill(gcolor);
      rect(x, y, 20-6, h, 50);
    }
    else {
      stroke(rcolor);
      fill(rcolor);
      rect(x, y, 20-6, h, 50);
    }

    let candidates = [random(-75, -150), random(75, 150)]
    line(x, y, x, y + random(candidates));
  }
}

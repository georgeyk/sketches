/*
Inktober 2021
Prompt: Knot
*/
function setup() {
  createCanvas(800, 600);
  background(198, 219, 240);
  noLoop();
}


function draw() {
  translate(400, 300);
  rotate(-HALF_PI);

  strokeWeight(2);
  fill(227, 216, 126);

  let angle = 0;
  let amplitude = 200;

  for (let i = 0; i < 850; i++) {
    x = cos(4*angle) * amplitude;
    y = sin(5*angle) * amplitude;

    rect(x, y, 20, 10, 25);

    angle += PI/300;
  }
}

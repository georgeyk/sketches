/*
  Inktober 2021
  Prompt: Pressure
*/

class Particle {
  constructor(x, y, size, color) {
    this.position = createVector(x, y);
    this.size = size;
    this.color = color;
  }

  update(nx, ny) {
    this.position.x += nx;
    this.position.y += ny;
  }

  show() {
    push();
    noStroke();
    fill(this.color);
    ellipse(this.position.x, this.position.y, this.size, this.size);
    pop();
  }
}


let main_particles;
let secondary_particles;

function setup() {
  createCanvas(800, 600);
  colorMode(HSB);

  main_particles = [];
  for (let i = 0; i < 500; i++) {
    let c = color(random(360), 90, 80);
    main_particles.push(new Particle(random(800), random(700, 1300), random(5, 10), c));
  }

  secondary_particles = [];
  for (let i = 0; i < 300; i++) {
    secondary_particles.push(new Particle(random(1, 200), random(500, 600), random(1, 5), color(0)));
  }
  frameRate(20);
}


function draw() {
  background(210, 3, 90);

  let change_phase  = true;
  for (particle of main_particles) {
    if (particle.position.y > 0) {
      change_phase = false;
      break;
    }
  }

  for (particle of main_particles) {
    particle.position.x = random(200, 400);

    if (change_phase) {
      particle.position.y = random(windowHeight, windowHeight*2);
    }

    if (particle.position.y < 420) {
      particle.update(random(-300, 400), -random(3));
    }
    else {
      particle.update(random(-1, 1), -random(5));
    }
    particle.show();
  }

  for (particle of secondary_particles) {
    particle.position.x = random(800);
    particle.update(random(-1, 1), random(-2, 2));
    particle.show();
  }

  push();
  strokeWeight(10);
  line(0, 450, 200, 450);
  line(400, 450, 800, 450);
  pop();
}

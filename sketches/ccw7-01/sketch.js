/*
George Y. Kussumoto
EDX - Creative Coding - NYUx DMEDX6063
Week 7 - Oct/2021

Prompt:
Create a sketch that moves multiple shapes along Lissajous curves.
In addition to animating the shapes’ x and y positions, use the sin() and cos() functions to animate their fill and/or stroke colors.
*/


const amplitude = 30;
let angle = 0;
// canvas
let bg;
let cx;
let cy;

function setup() {
  createCanvas(windowWidth, windowHeight);
  colorMode(HSB);
  rectMode(CENTER);
  frameRate(10);

  cx = windowWidth/2;
  cy = windowHeight/2;
  bg = color(225, 58, 40);
}


function draw() {
  background(bg);
  translate(cx, cy-200);

  strokeWeight(10);
  let grey = color(56, 6, 90);
  stroke(grey);

  // color component to create an pulsating effect
  let pulse_rate = map(sin(3*angle), -1, 1, 1, 100);

  // lissajous subject with lissajous sprinkles
  push();
  strokeWeight(1);
  fill(337, 24 + pulse_rate/2, 90);

  beginShape();
  for (let i = 0; i < TWO_PI; i+=0.1) {
    let x = sin(2*i) * amplitude;
    let y = cos(3*i) * amplitude;

    curveVertex(x, y);

    point(x - random(10), y - 10 + random(5));
    point(x + random(10), y + random(5));
    point(x - random(10), y + 10 + random(5));
  }
  endShape(CLOSE)
  pop();

  // "altar" subject
  fill(grey);

  rect(0, cy/3 + 10, 50, 90, 5);
  rect(0, cy/6, 75, 40, 50);

  line(-cx/2, cy/2, cx/2, cy/2);
  line(-cx/3, cy/2 + 25, cx/3, cy/2 + 25);

  // orbiting subjects in a different lissajous curve (so it's less boring)
  push();
  strokeWeight(5);
  stroke(337, 120-pulse_rate, 90);

  let px = sin(3*angle) * amplitude * 2;
  let py = cos(1*angle) * amplitude * 2;

  point(px, py);
  point(px/2, py);
  point(px, py/2);

  point(-px, -py);
  point(-px/2, -py);
  point(-px, -py/2);
  pop();

  // circular increment
  angle += 0.1;
  if (angle >= TWO_PI) {
    angle = 0;
  }
}

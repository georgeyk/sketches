/*
 Inktober 2021
 Prompt: Loop
*/

function setup() {
  createCanvas(800, 600);
  background(65);
  noLoop();
}

function draw() {
  translate(400, 260);
  let dark = color(57, 57, 58);
  let lime = color(221, 217, 42);
  let rd = color(225, 67, 81);

  strokeWeight(3);
  stroke(lime);

  fill(lime);
  ellipse(15, 30, 435, 435);

  fill(dark);
  ellipse(15, 30, 380, 380);

  rotate(HALF_PI/6);

  stroke(rd);
  fill(rd);
  triangle(195, 0, 245, 0, 218, 45);

  stroke(lime);
  fill(dark);

  for (let i = 0.3; i < TWO_PI; i+= 0.3) {
    let x = 200 * cos(i);
    let y = 200 * sin(i);
    rect(x, y, 40, 40);
  }
}

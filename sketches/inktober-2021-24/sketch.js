/*
 Inktober 2021
 Prompt: Extinct
*/
function setup() {
  createCanvas(800, 600);
  noLoop();
}

function draw() {
  background(237, 246, 249);

  strokeWeight(12);

  let c1 = color(0, 109, 119);
  stroke(c1);
  let leg1 = yline(300, true);

  let c2 = color(131, 197, 190);
  stroke(c2);
  let leg2 = yline(300);

  strokeWeight(0.5);

  for (let i = 0; i < 400; i++) {
    let d = leg2[i][0] - leg1[i][0];
    for (let k = 0; k < 10; k++) {
      let v1 = random(leg1[i][0], leg1[i][0] + d/2);
      push();
      stroke(c2);
      point(150 + v1, 50 + leg1[i][1]);

      stroke(c1);
      let v2 = random(leg2[i][0] - d/2, leg2[i][0]);
      point(150 + v2, 50 + leg2[i][1]);
      pop();
    }

  }

}

function yline(x, flip=false) {
  let angle = 0;
  let m = 60;
  let points = [];

  for (let y = 100; y < 500; y += 1) {
    let xy;
    if (!flip) {
      xy = x + cos(1.5*angle) * m;
    }
    else {
      xy = x - cos(1.75*angle) * m;
    }
    point(xy, y);
    points.push([xy, y]);

    angle += 0.015;
  }
  return points;
}

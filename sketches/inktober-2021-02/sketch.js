/*
This is a fork of https://openprocessing.org/sketch/1252670
In the repo, sketch ccw2-01.
*/
const suit = [
  [0, 3, 4, 0],
  [2, 4, 3, 1],
  [2, 4, 3, 1],
  [2, 4, 3, 1],
  [2, 4, 3, 1],
  [2, 4, 3, 1],
  [2, 0, 0, 1],
];

const max_rows = suit.length;
const max_cols = suit[0].length;
// we want to keep this proportion to be drawing consistent across distinct scales.
const min_col = 40;
const min_row = 60;
var scale_factor = 1;

function setup() {
  createCanvas(windowWidth, windowHeight);
  // the maximum that we can scale the drawing is the minimum
  // that we can fit on window width or height available
  let scale_w = Math.floor(windowWidth / (max_cols * min_col));
  let scale_h = Math.floor(windowHeight / (max_rows * min_row));
  scale_factor = Math.min(scale_w, scale_h);
}


function draw() {
  background(211, 211, 211);
  noStroke();
  var black = color(22, 26, 29);
  var red = color(102, 7, 8);

  // find the amount that we need to shift X and Y coordinates in order to draw
  // at the center of the canvas
  var row_step = min_row * scale_factor;
  var col_step = min_col * scale_factor;
  var move_x = (windowWidth - (col_step * max_cols)) / 2;
  var move_y = (windowHeight - (row_step * max_rows)) / 2;
  translate(move_x, move_y);

  // draw the matrix using our rules
  for (let row = 0; row < max_rows; row++) {
    for (let col = 0; col < max_cols; col++) {
      if (suit[row][col] == 1) {
        draw_half_rect(row, col, paint_left=true, black);
      }
      else if (suit[row][col] == 2) {
        draw_half_rect(row, col, paint_left=false, black);
      }
      else if (suit[row][col] == 3) {
        draw_half_rect(row, col, paint_left=true, red);
      }
     else if (suit[row][col] == 4) {
        draw_half_rect(row, col, paint_left=false, red);
      }
    }
  }

  noLoop();
}

function draw_half_rect(row, col, paint_left=true, paint_color) {
  // translate from the matrix indexes to canvas position
  var row_step = min_row * scale_factor;
  var col_step = min_col * scale_factor;
  var vertex_row = row * row_step;
  var vertex_col = col * col_step;

  push();
  fill(paint_color);
  beginShape();
  vertex(vertex_col, vertex_row);
  vertex(vertex_col + col_step, vertex_row);
  // at this point we have the top vertices of the rectangle set.
  // depending on "paint_left" value, we set the downwards Y coordinate
  // (y grows from top to bottom) to get the desired diagonal line.
  if (paint_left) {
    vertex(vertex_col, vertex_row + row_step);
  }
  else {
    vertex(vertex_col + col_step, vertex_row + row_step);
  }
  endShape();
  pop();
}

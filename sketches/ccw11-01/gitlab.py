"""
Script to fetch a list of commits and their respective stats provided by Gitlab API.
There's a external dependency to the requests library.
"""
import json
import requests

# doc: https://docs.gitlab.com/ee/api/#authentication
# You'll need to generate a access token in order to use the API
TOKEN = ''

# doc: https://docs.gitlab.com/ee/api/api_resources.html
since = '2021-10-01T00:00:00Z'
base_url = 'https://gitlab.com/api/v4/projects/georgeyk%2Fsketches/repository/commits'
final_url = f"{base_url}?private_token={TOKEN}&ref_name=main&since={since}&with_stats=true"

response = requests.get(final_url)
response.raise_for_status()
data = response.json()

final_data = []
# filter id and stats to the final json document
for commit in data:
    entry = {"id": commit["id"], "stats": commit["stats"]}
    final_data.append(entry)


# save it
with open("ccw11.json", "w+") as f:
    json.dump(final_data, f, indent=2)

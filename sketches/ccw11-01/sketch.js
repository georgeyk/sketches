/*
George Y. Kussumoto
EDX - Creative Coding - NYUx DMEDX6063
Week 11 - Nov/2021

Prompt:
Record 5-7 days of data from your life in a spreadsheet/CSV format. Suggested topics for data collection include:

- Cups of coffee consumed, productivity rating from 1-10
- # of steps climbed each day
- # of smiles per day, # of frowns per day
- Music streaming, genre/artist, time of day

Convert the data to JSON and save to .json file using the Convert CSV to JSON or Mr. Data Converter tools.
Using your .json file, visualize the data using p5.js.

---

Notes:

The dataset is a subset of commits made in the following repository:
- https://gitlab.com/georgeyk/sketches

To retrieve the commit information, there's an additional script (written in Python) available at:
- https://gitlab.com/georgeyk/sketches/-/blob/main/sketches/ccw11-01/gitlab.py

The script creates a "ccw11.json" file that is used in the sketch and also available in the repository
mentioned previously.
*/

const MAX_CHANGES = 250;
let commits;

function preload() {
  commits = loadJSON('ccw11.json');
}


function setup() {
  createCanvas(800, 800);
  noLoop();
}


function draw() {
  background(245, 203, 92);
  translate(400, 400);
  noFill();

  for (let commit of Object.values(commits)) {
    let additions = commit.stats.additions;
    let deletions = commit.stats.deletions;
    let total = commit.stats.total;

    // "total" is used as a scale, so bigger commits is visually represented by a longer radius
    // the arcs are then drawn to represent how much of this imaginary circle they represent,
    // and it's bounded to MAX_CHANGES

    // white arc
    stroke(250);
    let radd = map(additions, 0, MAX_CHANGES, 0, TWO_PI);
    arc(0, 0, total * 3, total * 3, PI, PI + radd);

    // black arc
    stroke(50);
    let rdel = map(deletions, 0, MAX_CHANGES, 0, TWO_PI);
    arc(0, 0, total * 3, total * 3, PI, PI + rdel);
  }
}

/*
 * Replication/study of Honoring Vera Molnár : (Des)Ordres
 * https://dam.org/museum/artists_ui/artists/molnar-vera/des-ordres/
 */

let g = {
	w: 720,
	h: 720,
	margin: 1,
	cols: 5,
	rows: 5,
	minRects: 5,
	maxRects: 20,
	useColors: false,
	useRounding: false,
	colors: [
		[0, 95, 115],
		[10, 147, 150],
		[148, 210, 189],
		[155, 34, 38],
		[155, 93, 229],
		[187, 62, 3],
		[238, 155, 0],
		[241, 91, 181],
		[254, 228, 64],
	],
};


function setup() {
	_config();

	createCanvas(g.w, g.h);
	noFill();
	background(233, 216, 166, 100);

	// tile
	g.tw = min(
		floor(g.w / (g.cols + (g.margin * 2))),
		floor(g.h / (g.rows + (g.margin * 2)))
	);
	g.th = g.tw;

	translate(g.tw * g.margin, g.th * g.margin);
	rectMode(CENTER);

	for (let i = 0; i < g.cols; i++) {
		for (let j = 0; j < g.rows; j++) {
			strokeWeight(random(0.5, 1.5));
			tile(
				g.tw * i, g.th * j, g.tw, g.th,
				g.useRounding ? random(2, 7) : 0
			);
		}
	}
}


function tile(x, y, w, h, r) {
	// 5%
	const lower = floor(g.w * 0.05);

	for (let i = 0; i < random(g.minRects, g.maxRects); i++) {
		if (g.useColors) {
			stroke(random(g.colors));
		}
		else {
			stroke(0, random(50, 150));
		}

		let s = random(w / lower, w);
		rect(x, y, s, s, r);
	}
}


function _config() {
	g.seed = random(1000);
	randomSeed(g.seed);

	g.w = floor(windowWidth);
	g.h = floor(windowHeight);

	g.useColors = random() > 0.5;
	g.useRounding = random() > 0.5;
	g.cols = floor(random(5, 21));
	g.rows = g.cols;

	console.log(g);
}


function debug(x, y, w, h) {
	push();
	stroke(150, 0, 0, 200);
	rect(x, y, w, h);
	pop();
}

/*
 Inktober 2021
 Prompt: Splat
*/
function setup() {
  createCanvas(800, 600);
  noLoop();
}

function draw() {
  background(0);
  let clrs = [
    color("#f72585"),
    color("#b5179e"),
    color("#7209b7"),
    color("#560bad"),
    color("#480ca8"),
    color("#3a0ca3"),
    color("#3f37c9"),
    color("#4361ee"),
    color("#4895ef"),
    color("#4cc9f0"),
  ];

  for (let i = 0; i < 99; i++) {
    splat(random(700), random(500), random(50, 300), random(clrs));
  }


  for (let i = 0; i < 11; i++) {
    let llx = random(800);
    let lly = random(600);
    splat(llx, lly, random(100), color(0));
  }

  for (let j = 0; j < 800; j += random(42)){
    strokeWeight(random(5));
    line(j, random(300), j, random(300, 600));
  }

}


function splat(x, y, r, clr) {
  push();
  translate(x, y);
  noStroke();
  fill(clr);

  beginShape();
  let seed = random(TWO_PI);
  for (let a = seed; a < seed + TWO_PI; a += 0.01) {
    let c = cos(a);
    let s = sin(a);

    let nx = map(c, -1, 1, 0, 1);
    let ny = map(s, -1, 1, 0, 1);
    let n = noise(nx, ny) * r;

    vertex(c * n, s * n);
  }
  endShape();
  pop();
}

/*
 Inktober 2021
 Prompt: Helmet
*/
function setup() {
  createCanvas(800, 600);
  background(248, 237, 235);
}

function draw() {
  translate(400, 250);
  noStroke();

  fill(106, 4, 15);
  ellipse(0, -25, 370, 300);

  push();
  rotate(-HALF_PI/4);
  rect(40, 0, 150, 350, 0, 0, 25, 25);
  rotate(PI/4);
  rect(-190, 0, 150, 350, 0, 0, 25, 25);
  pop();

  fill(231, 111, 81);
  ellipse(0, 0, 200, 100);
  ellipse(0, 0, 100, 200);

  rotate(-HALF_PI/2)
  fill(233, 196, 106);
  arc(0, -50, 100, 500, HALF_PI + HALF_PI/2, PI+HALF_PI);

  rotate(HALF_PI)
  fill(233, 196, 106);
  arc(0, -50, 100, 500, PI+HALF_PI, HALF_PI-HALF_PI/2);

  rotate(-HALF_PI/2)
  fill(244, 162, 97);
  ellipse(0, 0, 100, -100);
}
